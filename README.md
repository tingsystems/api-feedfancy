Install
-------

1.- Clone repo
```bash
$ cd 
$ mkdir webapps
$ cd webapps
$ git clone git@gitlab.com:tingsystems/api-feedfancy.git
```
2.- Create and active virtual env

```bash
$ cd
$ mkdir env
$ cd env/
$ virtualenv -p /usr/bin/python3 feedfancy
$ source feedfancy/bin/activate
```
3.- Install dependencies project
NOTE: In fedora install python3-devel, yum install -y gcc openssl-devel libyaml-devel libffi-devel readline-devel zlib-devel gdbm-devel ncurses-devel

```bash

$ cd 
$ cd webapps/api-feedfancy
$ pip install -r requirements.txt
``` 
4.- Start server

```bash
$ ./manage.py runserver

```
Testing queues and jobs
-------

Install redis https://redis.io

Enable watch messages on queue

```bash

$ ./manage.py rqworker  --settings=feedfancy.settings_local

```
Check stats 

```bash

$ ./manage.py rqstats  --interval=1  --settings=feedfancy.settings_local

```

Run with Docker
-------

1.- Docker Hub login
```bash
$ docker login --username foo --password-stdin
```

2.- Go to project path
```bash
$ cd go/to/api/project
```

3.- Build our image
```bash
$ docker build -t nameImage .
```

4.- Run a container
```bash
$ docker run -it -p 8000:8000 --name nameContainer nameImage
```

[Docker](https://docs.docker.com/get-started/ "About Docker")


Run with Docker Compose
-------


[Install docker-compose](https://docs.docker.com/compose/install/#install-compose "Install")

Change "DB_HOST" and "HOST_REDIS" for your IP in devEnv.list

1.- Build your images
```bash
$ docker-compose -f docker-compose-dev.yml build
```

2.- Run containers
```bash
$ docker-compose -f docker-compose-dev.yml up -d
```

3.- Check logs
```bash
$ docker-compose -f docker-compose-dev.yml logs -f
```
