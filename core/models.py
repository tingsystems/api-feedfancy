import uuid
from django.db import models
from django.contrib.postgres.fields import JSONField


class AbstractModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    createdAt = models.DateTimeField(auto_now_add=True, null=True)
    updatedAt = models.DateTimeField(auto_now=True)
    isActive = models.BooleanField(default=True)
    metadata = JSONField(null=True, blank=True)

    class Meta:
        abstract = True


class Taxonomy(AbstractModel):
    name = models.CharField(max_length=50)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.name


class Source(AbstractModel):
    name = models.CharField(max_length=100)
    url = models.URLField()
    taxonomies = models.ManyToManyField(Taxonomy)

    def __str__(self):
        return self.name


class SourceUrl(AbstractModel):
    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    url = models.URLField()
    taxonomies = models.ManyToManyField(Taxonomy)

    def __str__(self):
        return str(self.id)


class Post(AbstractModel):
    title = models.CharField(max_length=400)
    imgUrl = models.URLField()
    summary = models.TextField(null=True)
    link = models.URLField()
    content = models.TextField(null=True)
    taxonomies = models.ManyToManyField(Taxonomy)
    slug = models.SlugField(unique=True, max_length=500)
    source = models.ForeignKey(Source, null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        ordering = ['-createdAt']

    def __str__(self):
        return self.title
