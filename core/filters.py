from django.contrib.postgres.search import SearchVector
from functools import reduce
import django_filters
import tweepy
import yweather
from operator import __or__ as OR
from django.db.models import Q
from feedfancy.settings import consumer_key, consumer_secret, access_token, access_token_secret
from .models import Post


class TrendsFilter(django_filters.Filter):
    """

    Filter all post by twitter trends

    """

    def filter(self, qs, value):
        if value is None:
            return qs
        client = yweather.Client()
        WOEID = client.fetch_woeid(value)
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        api = tweepy.API(auth)
        trends1 = api.trends_place(WOEID)
        data = trends1[0]
        # grab the trends
        trends = data['trends']
        # grab the name from each trend
        names = [trend['name'].strip('#') for trend in trends]
        query = qs.annotate(
            search=SearchVector('summary', 'content', 'title')).filter(
            reduce(OR, (Q(search=c) for c in names))
        )
        return query


class PostFilter(django_filters.FilterSet):
    trends = TrendsFilter(name='trends')

    class Meta:
        model = Post
        fields = ('id', 'slug', 'createdAt', 'updatedAt', 'taxonomies', 'taxonomies__slug', 'trends')
