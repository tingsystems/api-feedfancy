from rest_framework.pagination import PageNumberPagination


class LargeResultsSetPagination(PageNumberPagination):
    page_size = None
    page_size_query_param = 'pageSize'
    max_page_size = None