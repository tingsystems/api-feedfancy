import requests
import validators
import tweepy
from django.template.defaultfilters import slugify
from bs4 import BeautifulSoup
from django_rq import job, enqueue
from core.shortener import Short
from core.models import Post, SourceUrl
from feedfancy.settings import consumer_key, consumer_secret, access_token, access_token_secret


def tweet(post):
    """

    :param post:
    :return:
    """

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)
    api.update_status(post.link + ' #feedfancy')
    # print(post.link + ' #feedfancy')


def string(strn):
    """
    método que elimina los espacios y separa el title de summary
    elimina los espacion antes y despues del ultimo caracter

    :param strn: string
    """
    strn = strn.strip()
    # busca los dos puntos retornando la posicion donde lo encontro
    x = strn.find(':')
    # si es la segunda posicion indica tiene que eliminar la hora
    if int(x) == 2:
        # corta la hora
        strn = strn[5:]
    # si no llego al final de la cadena
    elif int(x) != -1:
        # corta de los dos puntos hacia atras
        strn = strn[0:int(x) - 2]
    strn = strn.strip()
    return strn


@job
def boot_content(post, link, rules):
    """

    :param post:
    :param link:
    :param rules:
    :return:
    """
    r = requests.get(link)
    if r.status_code != 200:
        return
    soup = BeautifulSoup(r.text, 'html.parser')
    content = soup.find_all(True, {rules['parent'][0]: [rules['parent'][1]]}, limit=1, recursive=True)[0]
    text = ''
    for key, value in rules.items():
        # Exclude rules parent
        if key != 'parent':
            # Extracting data by rules and search
            for a in content.find_all(value[0]):
                # Build post by rules keys
                if value[1] == 'get':
                    post[key] = a.get(value[2])
                elif value[1] == 'text':
                    text += a.text
    post.content = text
    post.save()



@job
def boot(source_url):
    """

    :return:
    """
    r = requests.get(source_url.url)
    if r.status_code != 200:
        return
    soup = BeautifulSoup(r.text, 'html.parser')
    # get data by class main from source url
    rules = source_url.source.metadata['ruleList']
    links = soup.find_all(True, {rules['parent'][0]: [rules['parent'][1]]}, limit=rules['size'], recursive=True)
    # iter result find
    for l in links:
        post = {}
        # Apply rules for source url
        if not rules:
            continue
        for key, value in rules.items():
            # Exclude rules parent and size
            if key != 'parent' and key != 'size':
                # Extracting data by rules and search
                for a in l.find_all(value[0]):
                    # Build post by rules keys
                    if value[1] == 'get':
                        post[key] = a.get(value[2])
                    elif value[1] == 'text':
                        post[key] = a.text
        if validators.url(post['link']):
            if requests.get(post['link']).status_code != 200:
                continue
        link = post['link']
        post['link'] = Short.shorter(post['link'])
        # default url image
        img_default = 'https://dummyimage.com/600x400/ede6ed/1b1c1f.png&text=FeedFancy'
        if validators.url(post['img']):
            if requests.get(post['img']).status_code == 200:  # check if img url is valid
                img_default = post['img']  # add image url
        try:
            post = Post.objects.create(
                title=post['title'],
                link=post['link'].strip(),
                imgUrl=img_default,
                summary=post['summary'].strip(),
                slug=slugify(post['title']),
                source=source_url.source,

            )

            for t in source_url.taxonomies.all():
                post.taxonomies.add(t)
            enqueue(boot_content, post, link, source_url.source.metadata['ruleContent'])
        # enqueue(tweet, post)

        except:
            pass


@job
def send_spider():
    """
    Get source url periodic and enqueue for get data with the job boot
    :return:
    """
    uri = SourceUrl.objects.filter(isActive=True)
    for u in uri:
        if u.metadata is None or u.url is None:
            continue
        enqueue(boot, u)
